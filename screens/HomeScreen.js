import React from 'react';
import { WebView,BackHandler } from 'react-native';

export default class HomeScreen extends React.Component {
  webView = {
    canGoBack: false,
    ref: null,
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }
  
  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  }
  
  backHandler = () => {
    if(this.webView.canGoBack && this.webView.ref) {
        this.webView.ref.goBack();
        return true;
    }
    return false;
  }

  render() {
    return (
      <WebView
        ref={(webView) => { this.webView.ref = webView; }}
        source={{uri: 'http://pampecjr.com'}}
        onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack; }}
      />
    );
  }
}


